<?php

 class Bootstrap
 {
    protected $_params = array();
    
    public function __construct()
    {
        $this->extractUrl();

        $module = isset($this->_params['module']) && $this->_params['module'] != "" ? $this->_params['module'] : "default";
        $controller = isset($this->_params['controller']) && $this->_params['controller'] != "" ? $this->_params['controller'] : "welcome";
        $action = isset($this->_params['action']) && $this->_params['action'] != "" ? $this->_params['action'] : "index";
        require_once("application/modules/$module/controllers/$controller.php");
        $objController = new $controller;
        $objController->$action();              
    }

    protected function extractUrl()
    {
        #var_dump($_GET['url']);die;
        if(isset($_GET['url'])) {
            $url = explode('/', $_GET['url']);
            #var_dump($url);die;
            $this->_params['module'] = $url[0];
            $this->_params['controller'] = $url[1];
            $this->_params['action'] = $url[2];
        }
    }   
 }